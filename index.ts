import { 
    MatrixClient,
    SimpleFsStorageProvider,
    AutojoinRoomsMixin,
} from "matrix-bot-sdk";

const config = require("./config.json");

// config.json
const homeserverUrl = config.bot.homeserverUrl; 
const accessToken = config.bot.accessToken;

// In order to make sure the bot doesn't lose its state between restarts, we'll give it a place to cache
// any information it needs to. You can implement your own storage provider if you like, but a JSON file
// will work fine for this example.
const storage = new SimpleFsStorageProvider("stravnik-bot.json");

// Finally, let's create the client and set it to autojoin rooms. Autojoining is typical of bots to ensure
// they can be easily added to any room.
const client = new MatrixClient(homeserverUrl, accessToken, storage);
AutojoinRoomsMixin.setupOnClient(client);

// Before we start the bot, register our command handler
client.on("room.message", handleCommand);

// Now that everything is set up, start the bot. This will start the sync loop and run until killed.
client.start().then(() => console.log("Bot started!"));



// This is the command handler we registered a few lines up
async function handleCommand(roomId: string, event: any) {
    // Don't handle unhelpful events (ones that aren't text messages, are redacted, or sent by us)
    if (event['content']?.['msgtype'] !== 'm.text') return;
    if (event['sender'] === await client.getUserId()) return;
    
    // Check to ensure that the `!hello` command is being run
    const body = event['content']['body'];
    if (!body?.startsWith("!h")) return;
    
    // Now that we've passed all the checks, we can actually act upon the command
    await client.replyNotice(roomId, event, "Hello world!");

    let jidelnaId = "0000";

    let dataToSend = {cislo: jidelnaId, s5url: "", lang: "CZ"}

    
    const jidelnicek = await fetch("https://app.strava.cz/api/jidelnicky", {
        "credentials": "omit",
        "referrer": "https://app.strava.cz/jidelnicky?" + jidelnaId,
        "body": JSON.stringify(dataToSend),
        "method": "POST",
        "mode": "cors"
    });

    const json = await jidelnicek.text();

    client.sendNotice(roomId, json);

}

