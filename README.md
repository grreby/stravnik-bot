# stravnik-bot

Stravnik was a bot that should post meal lists from [strava.cz](https://strava.cz) to Matrix. For a working implementation, see [this](https://codeberg.org/tomkoid/stravnik).